import java.awt.Color;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class KrogiPanel extends JPanel {

	public KrogiPanel() {
		super();
	}
	
	
	public Dimension getPreferredSize() {
		return new Dimension(100, 100);
	}

	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if (SlikaPanel.slika != null){
			
			int SlikaWidth = SlikaPanel.slika.getWidth();
			int SlikaHeight = SlikaPanel.slika.getHeight();
			
			List<Integer> Xkoord = new ArrayList<Integer>();
			List<Integer> Ykoord = new ArrayList<Integer>();
			List<Color> color = new ArrayList<Color>();
			
			for (int i = 0; i < 500000; i++){
				Random r = new Random();
				
				int RW = r.nextInt(SlikaWidth);
				int RH = r.nextInt(SlikaHeight);
				
				Xkoord.add(RW);
				Ykoord.add(RH);
				
				
				color.add(PovprecnaBarva.Povprecna(SlikaPanel.slika, Xkoord.get(i), Ykoord.get(i), 5, 5));
				
				g.setColor(color.get(i));
				g.fillOval(Xkoord.get(i), Ykoord.get(i), 5, 5);
				
				}
			}
		
			else {
				g.setFont(new Font("Monaco", Font.PLAIN, 30));
				g.setColor(Color.BLACK);
				FontMetrics fm = g.getFontMetrics();
				String sporocilo = "Ni slike";
				int x = (getWidth() - fm.stringWidth(sporocilo))/2;
				int y = (getHeight() - fm.getHeight())/2;
				g.drawString(sporocilo, x, y);
				}
		}
}