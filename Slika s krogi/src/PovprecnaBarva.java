import java.awt.Color;
import java.awt.image.BufferedImage;


public class PovprecnaBarva {
	
	//BufferedImage slika - originalna slika
	//Integer (x0,y0) - sredi��e kroga, kjer ra�unamo povpre�no barvo
	//int (w,h) - oddaljenost od sredi��a v x in y smeri

	public static Color Povprecna(BufferedImage slika, Integer x0, Integer y0, int w,
	        int h) {
		
		int x1 = x0 + w;
	    int y1 = y0 + h;

	    long red = 0, green = 0, blue = 0;
	    
	    if (x1 > slika.getWidth() && y1 < slika.getHeight()) {
	    	for (int x = x0; x < slika.getWidth(); x++) {
		        for (int y = y0; y < y1; y++) {
			
			Color c = new Color(slika.getRGB(x, y));
			
			red += c.getRed();
			green += c.getGreen();
			blue += c.getBlue();
			}
		        }
	    	
	    }
	    
	    if (y1 > slika.getHeight() && x1 < slika.getWidth()) {
	    	for (int x = x0; x < x1; x++) {
	    		for (int y = y0; y < slika.getHeight(); y++) {
			
			Color c = new Color(slika.getRGB(x, y));
			
			red += c.getRed();
			green += c.getGreen();
			blue += c.getBlue();
			}
		        }
	    	
	    }
	    
	    if (y1 > slika.getHeight() && x1 > slika.getWidth()) {
	    	for (int x = x0; x < slika.getWidth(); x++) {
	    		for (int y = y0; y < slika.getHeight(); y++) {
			
			Color c = new Color(slika.getRGB(x, y));
			
			red += c.getRed();
			green += c.getGreen();
			blue += c.getBlue();
			}
		        }
	    	
	    }
	    
	    if (x1 <= slika.getWidth() && y1 <= slika.getHeight()) {
	    
	    for (int x = x0; x < x1; x++) {
	        for (int y = y0; y < y1; y++) {
		
		Color c = new Color(slika.getRGB(x, y));
		
		red += c.getRed();
		green += c.getGreen();
		blue += c.getBlue();
		}
	        }
	    }
	    
	    int num = h * w;
	    
	    Color randomColor = new Color((int)(red / num), (int)(green / num), (int)(blue / num));
	    
	    return randomColor;    
	}
}