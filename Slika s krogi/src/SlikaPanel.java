import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class SlikaPanel extends JPanel {
	static BufferedImage slika;
	
	
	public SlikaPanel() {
		super();
	}

	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(100, 100);
	}

	
	public void setSlika(BufferedImage slika) {
		SlikaPanel.slika = slika;
		repaint();
	}

	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if (slika != null) {
			int w = slika.getWidth();
			int h = slika.getHeight();
			int x = (this.getWidth() - w);
			int y = (this.getHeight() - h);
			g.drawImage(slika, x/2, y/2, w, h, Color.BLACK, null);
		}
		
		else {
			g.setFont(new Font("Monaco", Font.PLAIN, 30));
			FontMetrics fm = g.getFontMetrics();
			String sporocilo = "Ni originalne slike";
			int x = (getWidth() - fm.stringWidth(sporocilo))/2;
			int y = (getHeight() - fm.getHeight())/2;
			g.drawString(sporocilo, x, y);
		}
	}
}