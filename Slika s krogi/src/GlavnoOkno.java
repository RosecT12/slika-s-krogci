import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


@SuppressWarnings("serial")
public class GlavnoOkno extends JFrame {
	private SlikaPanel slika;
	private KrogiPanel krogi;
	private JButton gumb;
	private JButton gumb1;
	private JTextField urlText;
	private JLabel label;
	
	
	public GlavnoOkno() {
		super();
		
		// Nastavimo naslov okvirja
		setTitle("Slika s krogi");
		
		slika = new SlikaPanel();
		
		// Nastavimo layout manager
		setLayout(new GridLayout(1,0));
		
		// Dodamo sliko
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridy = 2;
		c.gridx = 0;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		c.anchor = GridBagConstraints.CENTER;
		add(slika, c);
		
		// Dodamo napis "URL Naslov"
		c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 0;
		label = new JLabel("URL naslov:");
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		slika.add(label);
		
		// Dodamo polje za vpis URL
		c = new GridBagConstraints();
		c.gridy = 1;
		c.gridx = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.gridwidth = 4;
		urlText = new JTextField();
		urlText.setPreferredSize(new Dimension(650,20));
		slika.add(urlText, c);
		
		// Dodamo gumb
		c = new GridBagConstraints();
		c.gridy = 1;
		c.gridx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.gridwidth = 2;
		gumb = new JButton("Nalo�i originalno sliko");
		gumb.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		gumb.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == gumb) {
					try {
						URL url = new URL(urlText.getText());
						BufferedImage s = ImageIO.read(url);
						slika.setSlika(s);
						}
					catch (IOException exc) {
						System.out.println("Bad URL");
						}
					}
				}
			}
		);
		
		slika.add(gumb, c);
		
		
		krogi = new KrogiPanel();

		// Dodamo sliko
		GridBagConstraints k = new GridBagConstraints();
		k.fill = GridBagConstraints.BOTH;
		k.gridy = 1;
		k.gridx = 1;
		k.weightx = 1.0;
		k.weighty = 1.0;
		k.gridwidth = GridBagConstraints.REMAINDER;
		k.anchor = GridBagConstraints.CENTER;
		add(krogi, k);

		// Dodamo gumb "PRETVORI"
		k = new GridBagConstraints();
		k.gridy = 0;
		k.gridwidth = 1;
		k.gridheight = 2;
		k.gridx = 0;
		gumb1 = new JButton("PRETVORI");
		gumb1.setFont(new Font("Tahoma", Font.BOLD, 25));
		
		gumb1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				krogi.repaint();
				}
			}
		);
		
		krogi.add(gumb1, k);
	}
}