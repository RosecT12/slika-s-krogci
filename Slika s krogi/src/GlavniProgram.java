import javax.swing.JFrame;

public class GlavniProgram {

	public static void main(String[] args) {
		JFrame okno = new GlavnoOkno();
		okno.setDefaultCloseOperation(GlavnoOkno.EXIT_ON_CLOSE);
		okno.pack();
		okno.setVisible(true);
	}
}