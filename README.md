# README #

![mačka.png](https://bitbucket.org/repo/Bjq4pe/images/771049458-ma%C4%8Dka.png)

### Za kaj se uporablja ta repozitorij? ###

* Program naloži črno-belo ali barvno sliko in iz nje naredi sliko s krogi

### Kako deluje program? ####

* Programu damo URL naslov slike in pritisnemo gumb 'Naloži originalno sliko'. Za tem kliknemo še gumb 'Pretvori', ki originalno sliko pretvori v sliko s krogi. Natančneje - na originalni sliki si izbere naključne točke, okoli katerih 'nariše' krog z določenim polmerom. V tem krogu izračuna povprečno barvo. Na drugi polovici zaslona nato okrog iste koordinate nariše krog z enakim polmerom in ga pobarva z izračunano povprečno barvo.


### Za dodatne informacije: ###

* Talita Rosec (talita.rosec@student.fmf.uni-lj.si)
* profesor Andrej Bauer